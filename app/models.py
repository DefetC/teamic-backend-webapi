# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC


# Includes Entities and Models that interact with DynamoDB
from datetime import datetime

from pynamodb.indexes import AllProjection, GlobalSecondaryIndex
from pynamodb.models import Model
from pynamodb.attributes import (UnicodeAttribute, UTCDateTimeAttribute,
                                 ListAttribute)

# Note: The following Indexes has been commented
#       They main use is to improve performance. They were commented out
#       as each index increase the team's cost.
# class SubjectIdIndex(GlobalSecondaryIndex):
#     """
#     Represents index for subject_id
#     """
#
#     class Meta:
#         read_capacity_units = 10
#         write_capacity_units = 5
#         projection = AllProjection()  # All attributes are projected
#
#     subject_id = UnicodeAttribute(hash_key=True)
#
#
# class WorkshopIdIndex(GlobalSecondaryIndex):
#     """
#     Represents index for workshop_id
#     """
#
#     class Meta:
#         read_capacity_units = 10
#         write_capacity_units = 5
#         projection = AllProjection()  # All attributes are projected
#
#     # The index's hash key, must also exist in the model
#     workshop_id = UnicodeAttribute(hash_key=True)
#
#
# class AssignmentIdIndex(GlobalSecondaryIndex):
#     """
#     Represents index for assignment_id
#     """
#
#     class Meta:
#         read_capacity_units = 10
#         write_capacity_units = 5
#         projection = AllProjection()  # All attributes are projected
#
#     # The index's hash key, must also exist in the model
#     assignment_id = UnicodeAttribute(hash_key=True)


class SubmissionModel(Model):
    """
    Represents a student assignment submission
    """

    class Meta:
        table_name = "submission"
        read_capacity_units = 5
        write_capacity_units = 5

    submission_id = UnicodeAttribute(hash_key=True)
    student_id = UnicodeAttribute(range_key=True)
    subject_id = UnicodeAttribute()
    workshop_id = UnicodeAttribute()
    assignment_id = UnicodeAttribute()

    # Indexes commented out. Read the note above (at the beginning of the file)
    # subject_id_index = SubjectIdIndex()
    # workshop_id_index = WorkshopIdIndex()
    # assignment_id_index = AssignmentIdIndex()

    comment = UnicodeAttribute(default="")
    docs = ListAttribute()
    date_created = UTCDateTimeAttribute(default=datetime.now())
    student_name = UnicodeAttribute()

    @staticmethod
    def get_submission(assignment_id: str, student_id: str):
        """
        Retrieve Submission from database for an assignment
        that belongs to a specified student
        :param assignment_id:
        :param student_id:
        :return:
        """
        result = None
        lst = SubmissionModel.scan(SubmissionModel.assignment_id
                                    .contains(assignment_id) &
                                    SubmissionModel.student_id
                                    .contains(student_id))

        # Should iterate only once as there should be only one result
        for sub in lst:
            result = sub

        return result

    @staticmethod
    def get_submission_by_id(submission_id: str):
        """
        Retrieve Submission from database by submission id
        :param submission_id:
        :return:
        """
        result = None
        lst = SubmissionModel.scan(
            SubmissionModel.submission_id.contains(submission_id))

        # Should iterate only once as there should be only one result
        for sub in lst:
            result = sub

        return result

    @staticmethod
    def get_submissions_by_subject(subject_id: str):
        """
        Retrieve all Submissions from database related to a subject
        :return:
        """

        return SubmissionModel.scan(
            SubmissionModel.subject_id.contains(subject_id))

    @staticmethod
    def get_submissions_by_workshop(workshop_id: str):
        """
        Retrieve all Submissions from database related to a workshop
        :param workshop_id:
        :return:
        """

        return SubmissionModel.scan(
            SubmissionModel.workshop_id.contains(workshop_id))
