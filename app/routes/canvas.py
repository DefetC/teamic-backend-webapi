# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Handles Submission API
import json
import requests
from flask import jsonify

from app.routes import bp
from config import Config


def look_up_workshop_id(subject_id, user_id):
    """
    this Api look up certain user's workshop id (section id in canvas) in certain subject
    :param subject_id: String
    :param user_id: String
    :return:
    """
    # TODO move this method into Class User
    url = Config.CANVAS_URL + "/api/v1/courses/" + subject_id \
          + "/enrollments?user_id=" + user_id
    # TODO store auth info into gitlab variable
    headers = {
        'Content-Type': 'application/json',
        'Authorization':
            'Bearer ' + Config.CANVAS_KEY
    }
    response = requests.get(url, headers=headers)
    json_list = json.loads(response.content.decode('utf-8'))

    workshop_id = 1
    for i in json_list:
        if i['course_section_id'] != 1:
            workshop_id = i['course_section_id']

    print(workshop_id)
    return workshop_id


@bp.route("/type/courses/<string:course_id>/users/<string:user_id>", methods=['GET'])
def enrollment_type(course_id, user_id):
    """
    this API is to return enrollment type given paras below
    :param course_id:
    :param user_id:
    :return: {'type' = enrollment_type: String}
    """
    type = look_up_type(course_id, user_id)
    reply = {'type': type}
    return jsonify(reply)


def look_up_type(course_id, user_id):
    """
    this method looks up certain enrollment type from canvas
    :param course_id:
    :param user_id:
    :return:
    """
    # TODO move this method into Class User
    url = Config.CANVAS_URL + "/api/v1/courses/" + course_id \
          + "/enrollments?user_id=" + user_id
    # TODO store auth info into gitlab variable
    headers = {
        'Content-Type': 'application/json',
        'Authorization':
            'Bearer ' + Config.CANVAS_KEY
    }
    print(url)
    response = requests.get(url, headers=headers)
    json_list = json.loads(response.content.decode('utf-8'))

    return json_list[0]['type']


@bp.route("/display/workshop/<courseId>/<userId>", methods=['GET'])
def look_up_workshop(courseId, userId):
    """
    this Api look up workshop ids for a specific user in a specific course
    :param course_id: String
    :param user_id: String
    :return:
    """
    urlToGetCourseName = Config.CANVAS_URL + "/api/v1/courses/{}".format(str(courseId))
    headersToGetCourseName = {
        'Content-Type': 'application/json',
        'Authorization':
            'Bearer ' + Config.CANVAS_KEY
    }
    responseToGetCourseName = requests.get(urlToGetCourseName, headers=headersToGetCourseName)
    jsonListToGetCourseName = json.loads(responseToGetCourseName.content)
    courseNameToBeElimianted = jsonListToGetCourseName["name"]

    url = Config.CANVAS_URL + "/api/v1/courses/{}/enrollments?role=TaEnrollment".format(str(courseId))
    headers = {
        'Content-Type': 'application/json',
        'Authorization':
            'Bearer ' + Config.CANVAS_KEY
    }

    response = requests.get(url, headers=headers)
    jsonList = json.loads(response.content)

    idList = list()
    namePair = []
    for info in jsonList:
        if "user_id" in info and info["user_id"] == int(userId) and "course_section_id" in info:
            idList.append(info["course_section_id"])

    # TODO to Haonan, add an api function to canvas.py instead of stats.py
    # for the part that interact with canvas
    for id in idList:
        url_section = Config.CANVAS_URL + "/api/v1/sections/{}".format(str(id))
        name_response = json.loads(requests.get(url_section, headers=headers).content)

        if "id" in name_response and name_response["id"] == int(id) and "name" in name_response:
            if name_response["name"] != courseNameToBeElimianted:
                temp = {"id": id, "name": name_response["name"]}
                namePair.append(temp)
    responseJson = {}
    responseJson["sections"] = namePair
    return json.dumps(responseJson)
