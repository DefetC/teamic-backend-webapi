# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Handles Submission API

import json
import os
import uuid
from datetime import datetime

import botocore
import requests
from flask import (request, jsonify, send_file)
from flask_json_schema import JsonSchema
from jsonschema import validate
from werkzeug.utils import secure_filename
from app.models import SubmissionModel
from app.routes.canvas import look_up_workshop_id
from config import Config
import boto3
from botocore.exceptions import NoCredentialsError, ClientError
from app.routes import bp

# ===========================================================
# Student submits
# Processes:
#     when front-end "submit" button pressed ->
#     * receive file from front-end and upload into S3
#     * receive submission information from front-end
#
# API calling consequence:
#     * POST /files: receiving files from front-end and upload to s3 bucket
#     * POST /submissions: real submitting happens
#
# Alternatively, student may want to review previous submissions, API called consequences:
#     * GET /submission/prev/check: for front-end deciding whether to display review button
#     * GET /submission/prev: transfer previous submission information
#     * GET /file/student: transfer files in previous submission
# ===========================================================
from manage import app

submission_post_schema = {
    "$schema": "https://json-schema.org/draft-06/schema#",
    "title": "submission-post-schema",
    "description": "Schema of receiving submissions from front-end via POST, used for /submissions",
    "type": "object",
    "properties": {
        "student_id": {
            "type": "string"
        },
        "student_name": {
            "type": "string"
        },
        "subject_id": {
            "type": "string"
        },
        "assignment_id": {
            "type": "string"
        },
        "files": {
            "type": "array"
        },
        "comment": {
            "type": "string"
        },
    },
    "required": ["student_id",
                 "subject_id",
                 "assignment_id",
                 "comment",
                 "files",
                 "student_name"
                 ]
}
schema = JsonSchema(app)


def rename_filename(student_id, assignment_id, prev_filename):
    """
    this method rename a file to insure that new filename is unique
    new filename is given based on timestamp, assignment id, student
    name and its previous filename
    :param student_id:
    :param assignment_id:
    :param prev_filename:
    :return:
    """
    timestamp = datetime.now()
    year = timestamp.strftime("%Y")
    month = timestamp.strftime("%m")
    day = timestamp.strftime("%d")
    time = timestamp.strftime("%H:%M:%S")
    new_filename = year + '-' + month + '-' + day + '-' + time + \
                   '-' + assignment_id + '-' + student_id + '-' + prev_filename
    return new_filename


@bp.route("/files", methods=['POST'])
def receive_document():
    """
    The first API for submitting.
    this API receive one file transmission from front-end,
    API will be called multiple times if there are more than
    one files in one submission.

    API function:
        * receive real file from front-end
        * upload received file into s3 bucket
        * delete uploaded file locally

    request body type: form-data

    request body:
        * file: <a real file>
        * assignment_id
        * student_id
    :return:
    """
    # retrieve information sent from front-end
    student_id = request.form['student_id']
    assignment_id = request.form['assignment_id']

    # define path
    file_dir = os.path.join('docs')
    # ensure file saved
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)
    file = request.files['file']

    # check whether *.pdf or *.txt
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        new_filename = rename_filename(student_id, assignment_id, filename)
        try:
            file.save(os.path.join(file_dir, new_filename))
            upload_to_s3(os.path.join(file_dir, new_filename), Config.S3_BUCKET_NAME, new_filename)
            os.remove(os.path.join(file_dir, new_filename))
            response = {'filename': new_filename}
            return jsonify(response)

        except NoCredentialsError as ee:
            return jsonify({'error': "Credentials not available"}), 400
    else:
        return jsonify({'error': 'invalid suffix'}), 400


def upload_to_s3(local_file_name, bucket_name, s3_file_name=None):
    """
    Upload a file to an S3 bucket
    :param local_file_name: File to upload
    :param bucket_name: Bucket to upload to
    :param s3_file_name: S3 object name. If not specified then local_file_name is used
    """
    s3 = boto3.client('s3')

    # If S3 object_name was not specified, use file_name
    if s3_file_name is None:
        s3_file_name = local_file_name

    s3.upload_file(local_file_name, bucket_name, s3_file_name)


def download_from_s3(bucket_name, s3_file_name):
    """
    Download a file from an S3 bucket
    :param bucket_name: Bucket to download from
    :param s3_file_name: S3 object name.
    """
    s3 = boto3.resource('s3')
    try:
        s3.Bucket(bucket_name).download_file(s3_file_name, s3_file_name)

    except botocore.exceptions.ClientError as e:

        if e.response['Error']['Code'] == "404":
            return jsonify({'error': 'file does not exist'}), 404
        else:
            raise


def allowed_file(filename):
    """
    this method checks whether suffix is 'pdf' or 'txt'
    :param filename: String
    :return: filename
    """
    ALLOWED_EXTENSIONS = {'txt', 'pdf'}
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@bp.route("/submissions", methods=['POST'])
def receive_submissions():
    """
    this API receives submission from front-end
    API call happens after all submission files have been received to back-end and uploaded to s3

    Processes:
        * check json received via schema
        * get all request body from front-end
        * assign attributes to submission (submission id and timestamp)
        * look up for workshop id from Canvas (course section id) and attach it to submission
        * forward submission detail to algorithm to check confidence numbers
        * update/store submission information into database

    Request body: student_name, student_id, assignment_id, subject_id, files, comment

    Forward to: <algo_url>/api/submissions/<filenames>
        with body submission_id, student_id, subject_id, assignment_id and workshop_id
        Because algorithm micro service uses customized url interpreter, example given below:
            * http://127.0.0.1:8000/api/submissions/file01.txt
            * http://127.0.0.1:8000/api/submissions/file01.txt;file02.txt
    """
    try:
        validate(request.json, submission_post_schema)
    except:
        return {"error": "invalid json"}, 400

    # extract data from POST request
    data = request.get_data().decode('utf-8')
    # encapsulated into dictionary
    dict_data = json.loads(data)

    # create timestamp and append to the dictionary
    dict_data['created_date'] = datetime.now()

    # create unique id via uuid and append to the dictionary
    submission_id = uuid.uuid1()
    dict_data['submission_id'] = str(submission_id)

    subject_id = dict_data['subject_id']
    user_id = dict_data['student_id']
    try:
        dict_data['workshop_id'] = str(look_up_workshop_id(subject_id, user_id))
    except:
        return {"error": "please register workshop first"}, 400
    """
    check if there were previous submissions under certain assignment
    if yes, receive files and update submission information;
    if no, receive files and store submission information
    """
    if SubmissionModel.get_submission(dict_data['assignment_id'], dict_data['student_id']) is None:
        store_submission(dict_data)
    else:
        update_submission(dict_data)
        delete_submission_to_algo(dict_data['submission_id'])

    # store_submission(dict_data)

    if (forward_to_algo(dict_data)) is True:
        return jsonify(dict_data), 200
    else:
        return {"error": "can not check confidence number",
                "advice": "please retry later"}, 400
    # return jsonify(dict_data)


def forward_to_algo(dict_data):
    """
    this method forward submission information to algorithm micro-service
    :param dict_data: a dictionary which contains all information regarding one submission
    :return:
    """
    files = dict_data['files']
    files_url = ""
    for filename in files:
        files_url = files_url + filename + ';'
    # TODO move URL into GitLab variable
    base_url = Config.ALGO_URL + '/api/submissions/'
    url = base_url + files_url
    url = url[:-1]
    print('forward to algorithm ' + url)
    headers = {
        'Content-Type': 'application/json'
    }
    json_data = {
        "submission_id": dict_data['submission_id'],
        "student_id": dict_data['student_id'],
        "workshop_id": dict_data['workshop_id'],
        "assignment_id": dict_data['assignment_id'],
        "subject_id": dict_data['subject_id'],
    }

    try:
        response = requests.post(url, data=json.dumps(json_data), headers=headers)
        print(f'Response Code Received from Algo: {response.status_code}')
        if response.status_code is 202:
            return True
        else:
            return False
    except Exception as e:
        print("Error with sending request to Algo service")
        print(e)
        return False


def update_submission(dict_data):
    """
    update submission information in db and algo
    :param dict_data:
    :return:
    """
    assignment_id = dict_data['assignment_id']
    student_id = dict_data['student_id']
    submission = SubmissionModel.get_submission(assignment_id, student_id)
    submission_id = submission.submission_id
    submission.delete()
    dict_data['submission_id'] = submission_id
    store_submission(dict_data)



def delete_submission_to_algo(submission_id):
    """
    this method sends a delete request to algo to delete
    a previous submission information
    :param submission_id:
    :return:
    """
    url = Config.ALGO_URL + '/api/submissions/delete?submission_id=' + submission_id
    try:
        response = requests.put(url)
        if response.status_code is 202:
            return True
        else:
            return False
    except:
        return False


def store_submission(dict_data):
    """
    this method stores submission information into db
    :param dict_data: received json from front-end
    :return:
    """
    sub: SubmissionModel = SubmissionModel()
    sub.submission_id = dict_data['submission_id']
    sub.student_id = dict_data['student_id']
    sub.subject_id = dict_data['subject_id']
    sub.workshop_id = dict_data['workshop_id']
    sub.assignment_id = dict_data['assignment_id']
    sub.comment = dict_data['comment']
    sub.date_created = dict_data['created_date']
    sub.docs = dict_data['files']
    sub.student_name = dict_data['student_name']
    sub.save()


@bp.route("/submission/prev", methods=['GET'])
def search_filenames():
    """
    This API returns filename with given student id and assignment id
    :return:
    """
    student_id = request.args.get('student_id')
    assignment_id = request.args.get('assignment_id')
    result = SubmissionModel.get_submission(assignment_id, student_id).docs
    return jsonify({'files': result})


@bp.route("/submission/prev/check", methods=['GET'])
def check_prev_submissions():
    """
    This API returns filename with given student id and assignment id
    :return:
    """
    student_id = request.args.get('student_id')
    assignment_id = request.args.get('assignment_id')
    result = SubmissionModel.get_submission(assignment_id, student_id)
    if result is None:
        return jsonify({'result': False})
    else:
        return jsonify({'result': True})


@bp.route("/file/student", methods=['GET'])
def retrieve_file():
    """
    this API returns required file according to filename
    :return:
    """
    file = request.args.get('filename')
    download_from_s3(Config.S3_BUCKET_NAME, file)
    file_path = os.path.join(app.root_path, "../" + file)

    try:
        ret = send_file(file_path)
        return ret
    except Exception as e:
        return str(e)
    finally:
        os.remove(file_path)

# ===========================================================
#  Staff specific APIs and helper functions
#  all information displayed in table view is achieved from GET /submissions
# ===========================================================

@bp.route("/submissions", methods=['GET'])
def query_submissions():
    """
    this method query submission information from db
    according to information provided by front-end.

    Param: subject_id, workshop_id, assignment_id

    Target url of algorithm micro service example:
        * query by subject http://127.0.0.1:8000/api/submissions/subject?subject_id=sct1
        * query by workshop http://127.0.0.1:8000/api/submissions/workshop?workshop_id=ws1
    :return:
    """
    # retrieve required information from request
    subject_id = request.args.get('subject_id')
    workshop_id = request.args.get('workshop_id')
    assignment_id = request.args.get('assignment_id')

    # query submission & confidence number information from algorithm
    response = query_from_algo(workshop_id, subject_id)

    if 'data' not in response:
        return jsonify({'data': []})

    del response['message']
    records = response['data']

    for index in range(len(records) - 1, -1, -1):
        submission_id = records[index]['submission_id']
        submission = SubmissionModel.get_submission_by_id(submission_id)
        if submission is None:
            records.remove(records[index])
            continue
        elif assignment_id is not None and records[index]['assignment_id'] != assignment_id:
            records.remove(records[index])
            continue
        records[index]['created_date'] = submission.date_created
        records[index]['student_name'] = submission.student_name

        for file in records[index]['files']:
            del file['text']
            confidence_number = file['metrics'][0]['metric']
            del file['metrics']
            file['confidenceNumber'] = confidence_number

    return jsonify(response)


def query_from_algo(workshop_id, subject_id):
    """
    this method sends requests to algorithm to get all the
    submission information
    :param workshop_id:
    :param subject_id:
    :return:
    """
    base_url = Config.ALGO_URL + '/api/submissions/'
    url = ''
    if workshop_id is None:
        url = base_url + "subject?subject_id=" + subject_id
    elif subject_id is None:
        url = base_url + "workshop?workshop_id=" + workshop_id

    print("query_from_algo " + url)
    response = requests.get(url)
    return response.json()


@app.route("/submissions/approve", methods=['POST'])
def approve_assignments():
    """
    This api allows the instructor to approve a list of submissions
    that the instructor has marked as approved.
    :param list of submission_id
    :return:
    """
    # try:
    #     validate(request.json, approve_reject_schema)
    # except:
    #     return {"error": "invalid json"}
    # extract data from POST request
    base_url = Config.ALGO_URL + '/api/submissions/validate'
    data = request.get_data().decode('utf-8')
    # encapsulated into dictionary
    dict_data = json.loads(data)
    for submission_id in dict_data['submission_id']:
        url = base_url + '?submission_id=' + submission_id + '&valid=' + 'true'
        try:
            response = requests.put(url)
        except:
            return {"message": "error"}, 400
    return {"message": "received"}, 200


@app.route("/submissions/reject", methods=['POST'])
def reject_assignments():
    """
    This api allows the instructor to approve a list of submissions
    that the instructor has marked as rejected.
    :param list of submission_id
    :return:
    """
    # try:
    #     validate(request.json, approve_reject_schema)
    # except:
    #     return {"error": "invalid json"}

    base_url = Config.ALGO_URL + '/api/submissions/validate'
    # extract data from POST request
    data = request.get_data().decode('utf-8')
    # encapsulated into dictionary
    dict_data = json.loads(data)
    for submission_id in dict_data['submission_id']:
        url = base_url + '?submission_id=' + submission_id + '&valid=' + 'false'
        try:
            response = requests.put(url)
        except:
            return {"message": "error"}, 400
    return {"message": "received"}, 200
