import json

from app.routes import bp
import json
from statistics import mean, median
import requests
from app.routes import bp
from manage import app
from config import Config


@bp.route("/words/subject/<subject_id>/assignment/<assignment_id>", methods=['GET'])
def retrieve_words(subject_id, assignment_id):
    url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subject_id
    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8').replace('\0', ''))
    # json_list = json.loads(response.content.decode('utf-8'))
    # dir(response)
    # scores is a list of submission scores
    submission_num = 0
    sentences = []
    if "data" in json_list:
        for submission in json_list["data"]:

            if not assignment_id == submission['assignment_id']:
                continue

            if "files" in submission and len(submission["files"]) != 0:

                for file in submission["files"]:

                    if "sentences" in file:
                        sentences = file["sentences"]
    results = dict()
    results["subjectId"] = str(subject_id)
    results["stats"] = {}
    results['stats']['sentences'] = []
    for wordMap in sentences:
        results['stats']['sentences'].append(wordMap)
    return json.dumps(results)

@bp.route("/algorithm/subject/<subject_id>/assignment/<assignment_id>", methods=['GET'])
def retrieve_all_algotithms(subject_id, assignment_id):
    url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subject_id
    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8').replace('\0', ''))
    # json_list = json.loads(response.content.decode('utf-8'))
    # dir(response)
    # scores is a list of submission scores
    submission_num = 0
    sentences = []
    scores = dict()
    if "data" in json_list:
        for submission in json_list["data"]:

            if not assignment_id == submission['assignment_id']:
                continue

            if "files" in submission and len(submission["files"]) != 0:

                for file in submission["files"]:
                    if "metrics" in file:
                        for metric in file["metrics"]:
                            if "metric" in metric:
                                scores[metric['metric_name']] = metric['metric']
    results = dict()
    results["subjectId"] = str(subject_id)
    results["stats"] = scores
    return json.dumps(results)
