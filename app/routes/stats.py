# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Handles Submission API
import json
from statistics import mean, median
import requests
from app.routes import bp
from manage import app
from config import Config

#TODO add proper commenting
#TODO change function and variable names. Follow conventions in submissions.py


@bp.route("/stats/subject/<subject_id>/assignment/<assignment_id>", methods=['GET'])
def retrieve_subject_assignment_stats(subject_id, assignment_id):
    """
    this Api look up stats information for a specific subject
    :param subjectId: String
    :return:
    """
    url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subject_id
    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8').replace('\0', ''))
    #json_list = json.loads(response.content.decode('utf-8'))
    #dir(response)
    # scores is a list of submission scores
    scores = []
    submission_num = 0

    if "data" in json_list:
        for submission in json_list["data"]:

            if not assignment_id == submission['assignment_id']:
                continue

            submission_metric = 0
            submission_num += 1

            if "files" in submission and len(submission["files"])!=0 :

                for file in submission["files"]:
                    if "metrics" in file:
                        for metric in file["metrics"]:
                            if "metric" in metric and metric['metric_name'] == "cosine_similarity":
                                submission_metric = metric["metric"]
            scores.append(submission_metric)


    results = dict()
    results["subjectId"] = str(subject_id)
    results["stats"] = {}

    if len(scores) != 0:
        results["stats"]["highest"] = max(scores)
        results["stats"]["lowest"] = min(scores)
        results["stats"]["average"] = mean(scores)
        results["stats"]["median"] = median(scores)
        results["stats"]["totalNum"] = submission_num
    else:
        results["stats"]["highest"] = 0
        results["stats"]["lowest"] = 0 
        results["stats"]["average"] = 0
        results["stats"]["median"] = 0
        results["stats"]["totalNum"] = submission_num

    return json.dumps(results)

    # else:
    #     abort(404, message="Statistics of subject {} doesn't exist".format(subjectId))


@bp.route("/stats/workshop/<workshop_id>/assignment/<assignment_id>", methods=['GET'])
def retrieve_workshop_assignment_stats(workshop_id, assignment_id):
    """
    this Api look up stats infromation for a specific workshop
    :param workshopId: String
    :return:
    """
    url = Config.ALGO_URL + "/api/submissions/workshop?workshop_id=" + workshop_id

    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8'))

    
    # scores is a list of submission scores
    scores = []
    sentence=[]
    submission_num = 0

    if "data" in json_list:
        for submission in json_list["data"]:

            if not assignment_id == submission['assignment_id']:
                continue

            submission_metric = 0
            submission_num += 1

            if "files" in submission and len(submission["files"])!=0 :

                for file in submission["files"]:
                    if "metrics" in file:
                        for metric in file["metrics"]:
                            if "metric" in metric and metric['metric_name'] == "cosine_similarity":
                                submission_metric = metric["metric"]
            scores.append(submission_metric)


    results = dict()
    results["workshopId"] = str(workshop_id)
    results["stats"] = {}

    if len(scores) != 0:
        results["stats"]["highest"] = max(scores)
        results["stats"]["lowest"] = min(scores)
        results["stats"]["average"] = mean(scores)
        results["stats"]["median"] = median(scores)
        results["stats"]["totalNum"] = submission_num
    else:
        results["stats"]["highest"] = 0
        results["stats"]["lowest"] = 0 
        results["stats"]["average"] = 0
        results["stats"]["median"] = 0
        results["stats"]["totalNum"] = submission_num

    return json.dumps(results)

    # else:
    #     abort(404, message="Statistics of workshop {} doesn't exist".format(workshopId))

# ===========================================================================================
@bp.route("/stats/subject/<subjectId>", methods=['GET'])
def retrieve_subject_stats(subjectId):
    """
    this Api look up stats information for a specific subject
    :param subjectId: String
    :return:
    """

    url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subjectId
    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8'))

    # scores is a list of submission scores
    scores = []
    submission_num = 0

    if "data" in json_list:
        for submission in json_list["data"]:
            submission_metric = 0
            submission_num += 1

            if "files" in submission and len(submission["files"])!=0 :

                for file in submission["files"]:
                    if "metrics" in file:
                        for metric in file["metrics"]:
                            if "metric" in metric and metric['metric_name'] == "cosine_similarity":
                                submission_metric = metric["metric"]

            scores.append(submission_metric)

    results = dict()
    results["subjectId"] = str(subjectId)
    results["stats"] = {}

    if len(scores) != 0:
        results["stats"]["highest"] = max(scores)
        results["stats"]["lowest"] = min(scores)
        results["stats"]["average"] = mean(scores)
        results["stats"]["median"] = median(scores)
        results["stats"]["totalNum"] = submission_num
    else:
        results["stats"]["highest"] = 0
        results["stats"]["lowest"] = 0
        results["stats"]["average"] = 0
        results["stats"]["median"] = 0
        results["stats"]["totalNum"] = submission_num

    return json.dumps(results)

    # else:
    #     abort(404, message="Statistics of subject {} doesn't exist".format(subjectId))


@bp.route("/stats/workshop/<workshopId>", methods=['GET'])
def retrieve_workshop_stats(workshopId):
    """
    this Api look up stats infromation for a specific workshop
    :param workshopId: String
    :return:
    """
    url = Config.ALGO_URL + "/api/submissions/workshop?workshop_id=" + workshopId

    response = requests.get(url)
    json_list = json.loads(response.content.decode('utf-8'))

    # scores is a list of submission scores
    scores = []
    submission_num = 0

    if "data" in json_list:
        for submission in json_list["data"]:
            submission_metric = 0
            submission_num += 1

            if "files" in submission and len(submission["files"])!=0 :

                for file in submission["files"]:
                    if "metrics" in file:
                        for metric in file["metrics"]:
                            if "metric" in metric and metric['metric_name'] == "cosine_similarity":
                                submission_metric = metric["metric"]

            scores.append(submission_metric)

    results = dict()
    results["workshopId"] = str(workshopId)
    results["stats"] = {}

    if len(scores) != 0:
        results["stats"]["highest"] = max(scores)
        results["stats"]["lowest"] = min(scores)
        results["stats"]["average"] = mean(scores)
        results["stats"]["median"] = median(scores)
        results["stats"]["totalNum"] = submission_num
    else:
        results["stats"]["highest"] = 0
        results["stats"]["lowest"] = 0
        results["stats"]["average"] = 0
        results["stats"]["median"] = 0
        results["stats"]["totalNum"] = submission_num

    return json.dumps(results)

    # else:
    #     abort(404, message="Statistics of workshop {} doesn't exist".format(workshopId))

