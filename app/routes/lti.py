# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Handles routes related to LTI connection
import json
from flask import (render_template, session, request, Response,
                   send_from_directory, jsonify)
from pylti.flask import lti


# TODO add commenting
from app.routes import bp
from manage import app


def return_error(msg):
    return render_template('error.htm.j2', msg=msg)


def error(exception=None):
    # TODO do proper logging
    # app.logger.error("PyLTI error: {}".format(exception))
    return send_from_directory('./sites/authorized', 'index.html')


@bp.route('/<path:path>', methods=['GET'])
def static_proxy(path):
    return send_from_directory('./sites/authorized', path)


# LTI Launch
@bp.route('/launch', methods=['POST', 'GET'])
@lti(error=error, request='initial', role='any', app=app)
def launch(lti=lti):
    """
    Returns the launch page
    request.form will contain all the lti params
    """

    # example of getting lti data from the request
    # let's just store it in our session
    session['lis_person_name_full'] = request.form.get('lis_person_name_full')
    User.user_id = request.form.get('custom_canvas_user_id')
    User.email = request.form.get('lis_person_contact_email_primary')
    User.name = request.form.get('lis_person_name_full')

    User.roles = request.form.get('roles')

    # Write the lti params to the console
    app.logger.info(json.dumps(request.form, indent=2))

    # TODO uncomment and check why it causes a problem
    # lis_person_name_full = session['lis_person_contact_email_primary']
    # print(lis_person_name_full)

    return send_from_directory('./sites/authorized', 'index.html')


# Home page
@bp.route('/', methods=['GET', 'POST'])
def index(lti=lti):
    return render_template('index.htm.j2')


# LTI XML Configuration
@bp.route("/xml/", methods=['GET'])
def xml():
    """
    Returns the lti.xml file for the app.
    XML can be built at https://www.eduappcenter.com/
    """
    try:
        return Response(render_template(
            'lti.xml.j2'), mimetype='application/xml'
        )
    except:
        app.logger.error("Error with XML.")
        return return_error('''Error with XML. Please refresh and try again. If this error persists,
        please contact support.''')


class User(object):
    """
    this class describes a user
    """
    user_id = ""
    name = ""
    email = ""
    enrollment_type = ""
    #enrollment_type = ""


@bp.route("/user", methods=['GET'])
def return_user_info():
    """
    this API return all information regarding current user
    because front-end knows nothing about who the user is
    :return:
    """
    #data = {'user_id': User.user_id, 'name': User.name, 'email': User.email}
    data = {'user_id':User.user_id, 'name': User.name, 'email': User.email}
    return jsonify(data)
