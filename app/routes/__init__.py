from flask import Blueprint

bp = Blueprint('bp', __name__)
from app.routes import canvas, lti, stats, submission, words
