# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Responsible for bootstrapping the backend web api
from flask import Flask
from flask_cors import CORS
from config import config


def create_app(config_name):
    """
    Flask app factory. Used to create an instance of the application
    :param config_name Reference to configuration object to apply.
    Configurations can be found in config.py
    :return:
    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    CORS(app)

    return app


# ============================================
# Logging
# ============================================

# TODO refactor formatting. It must be enabled through AWS Cloud Watch
# Log configuration must be put in config file
# LOG_FILE = 'error.log'
# LOG_FORMAT = '%(asctime)s [%(levelname)s] {%(filename)s:%(lineno)d} %(message)s'
# LOG_LEVEL = 'INFO'
# LOG_MAX_BYTES = 1024 * 1024 * 5  # 5 MB
# LOG_BACKUP_COUNT = 1
# formatter = logging.Formatter(settings.LOG_FORMAT)
# handler = RotatingFileHandler(
#     settings.LOG_FILE,
#     maxBytes=settings.LOG_MAX_BYTES,
#     backupCount=settings.LOG_BACKUP_COUNT
# )
# handler.setLevel(logging.getLevelName(settings.LOG_LEVEL))
# handler.setFormatter(formatter)
# app.logger.addHandler(handler)

from app import models