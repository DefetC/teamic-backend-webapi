# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Main file where the server starts
import os
from app import create_app

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

# Add routing through Blue Print
from app.routes import bp
app.register_blueprint(bp, url_prefix='/')
