# Overview
This repository contains the backend responsible for providing the web api service for the Integrity Checking Project from Software Project (COMP90082_2020_SM2) at University of Melbourne, 2020.

# Deploy Webapi Service on AWS Server

1. Firstly you need to browse DevOps repository and set up DevOps environment on AWS server.

2. You should build front end repository and deploy Algo service begore deploy Webapi service.

3. Create a new repository on Gitlab. Upload the source code to this repository.

4. The pipeline of Gitlab should automatically run, but it will meet the problem at this step. We need firstly set the environment variable.

5. Set environment variable:

   1. Go to home page of the gitlab repository

   2. Go to Settings → CI/CD

   3. Find Variables and click Expand. You need fill in environment variables here.

   4. The following are the variables

   5. 1. | Variable name         | Variable value                                               | Sample                                                       | Note                              |
         | :-------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :-------------------------------- |
         | ALGO_URL              | http://<AWS_service_server>:8000                             | http://54.66.32.135:8000                                     |                                   |
         | APP_HOST              | 0.0.0.0                                                      |                                                              |                                   |
         | APP_PORT              | 5000                                                         |                                                              |                                   |
         | AWS_ACCESS_KEY_ID     | <[AWS key id](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | AKIA5RJHJOKXKJQJT3MD                                         |                                   |
         | AWS_DB_HOST           | http://dynamodb.ap-southeast-2.amazonaws.com/                |                                                              |                                   |
         | AWS_REGION            | ap-southeast-2                                               |                                                              |                                   |
         | AWS_S3_BUCKET         | <[AWS Bucket name](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | teamic-s3-bucket-cosmicfish                                  |                                   |
         | AWS_SECRET_ACCESS_KEY | <[AWS secret key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | jzoHeWngStp2ayTwEEqqcK0IYFiQWHJdRAjLC1FF                     |                                   |
         | CANVAS_KEY            | <[Develop token](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Canvas+Configuration)> | 7kF6N9e13BOqLJrlX0ePmlR6hiEKoVSUemVmNR1gGXNmxQkkWIHDgPXemxhknaxt |                                   |
         | CANVAS_URL            | http://<[Canvas server ip](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Setup+Canvas+Server)>:3000 | http://3.25.127.42:3000                                      |                                   |
         | CONSUMER_KEY          | <[Consumer key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Canvas+Configuration)> | 10000000000001                                               |                                   |
         | SHARED_SECRET         | <[Shared Secret Key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Canvas+Configuration)> | NnQuIa72IRxz2VUZ8rXCjT7eiueBGtIwvKklLKGbiUTUB5eIMVE3fba27d1gHulh |                                   |
         | DEPLOY_HOST           | <[AWS Service server](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | 54.66.32.135                                                 |                                   |
         | FE_IMAGE              | <[Front end repo](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Build+Frontend+Repository)> | registry.gitlab.com/defetc/swen90013-team-ic-front-end       |                                   |
         | FE_REGISTERY_PASEWORD | more detail in following                                     | 7yysAsXzLCtyNeHnyKra                                         |                                   |
         | FE_REGISTRY_USER      | gitlab-deploy                                                | gitlab-deploy                                                |                                   |
         | PRIVATE_SSH_KEY       | <[AWS ssh private key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | -----BEGIN RSA PRIVATE KEY-....                              | This value should be set to File. |

      2. FE_REGISTERY_PASEWORD/FE_REGISTRY_USE: These two variable is set in "Access Tokens" in "User setting" of GitLab account.   FE_REGISTERY_PASEWORD is Personal Access Token, FE_REGISTRY_USER is the name of Personal Access Token.

   6.  Run the pipeline again. This time it will success and build the webapi service in AWS server.

# Structure
The repo includes backend files only. Since the flask app needs to include
frontend artifacts, this repo can be run in development mode when these artifacts
are added manually.
When being built by pipeline, these artifacts are added automatically.

## Adding Frontend Artifacts
1. Follow the Frontend repository README instructions to know how to compile
the product.
2. After compilation, copy the sites folder in Frontend root repository and place 
it inside /app folder.

## Related files and folders

### manage.py
Responsible for running the web app

### app/__init__.py
Bootstrap the web app

### app/routes Folder
Contains controllers for different services provided by the application including:
* **canvas.py:** All API Calls to Canvas
* **lti.py:** All related API settings to authenticate using LTI standard
* **stats.py:** API Services used by Frontend that provides statistical information
of submissions
* **submission.py:** API Services used by Frontend that provide all functionality
related to submitting an assignment, retrieving it... etc

### app/models.py
Contains model that connects to DynamoDB

# Deploy Webapi Service locally

NOTE: Local installation methods are legacy methods left by previous teams. We strongly recommend that you use pipeline to install project in AWS server instead.

## Requirements

* Python 3.7
* Pip
* Docker

## Setup

### 1. Create a virtual environment
```
python3 -m venv venv
```
This will create a virtual environment under the folder venv

### 2. Activate the virtual environment 
```
source venv/bin/activate
```

**Tip:** Having a autoenv tool installed will help you to avoid activating 
your virtual machine whenever you go to the repository folder. More information
can be found from: https://github.com/inishchith/autoenv

### 3. Install Pipenv
The project uses pipenv to help manage all dependencies and make it portable
 between team members. Run the following command (inside the activated virtual 
 environment):

```
pip install pipenv
```

### 4. Install dependencies
```
pipenv install
```

### 5. Configure AWS CLI
This is needed only once and helps to connect the project to real AWS environment.
Read the section **AWS CLI Configuration** for more information.

## AWS CLI Configuration
To access AWS S3 and DynamoDB services, you need to obtain credentials (Access key and secret).
This can be obtained by using IAM from AWS console. It is, however, necessary to
configure aws cli even if no real key is provided for development purposes. Without
it, the used aws libraries (boto3 for S3 and pynamodb for Dynamodb) will fail.
When in development mode, you can provide dummy non real information. However,
it is better to provide real credentials from the start so you can run the system
on production environment on localhost for testing purposes. <br>
To configure aws cli, run the following command:
```
aws configure
```
You'll be asked to provide:
1. **aws_access_key_id:** Provide real or dummy key
2. **aws_secret_access_key:** Provide real or dummy ,ey
3. **default_region_name:** Should be one of AWS regions. The system was built
                            in Australia, so default region was 'ap-southeast-2'
4. **output_format:**  Should be blank, just press enter.


# Running Server
The server can run in three modes:
* Development Mode
* Testing Mode
* Production Mode

All of these modes relies on:
1. Activated Virtual Environment
2. Running Algo Service
3. Settings(environment variables) related to the mode
4. Having a working Canvas Environment that is deployed and accessed through 
the environment. 



### 1. Activating Virtual Environment
After initial setup, a virtual environment and basic settings are complete. 
Activating the virtual environment again is done simply by running:
```
source venv/bin/activate
```

### 2. Running Algo Service
Please refer to backend-algo repository on how to run the service.

### 3. Settings related to the Mode 
Each mode needs different environment variables before running the system.
Please refer to each mode's section for more details.

### Running the server
Make sure to be inside an activated virtual envrionment. Then add the environment variables
 that is suitable for the mode you want to run the server on. Following that, run
 the command:
```
gunicorn manage:app -b localhost:5000
```

## Development Mode
By default, the development mode runs the web-api service on default settings which are
```
FLASK_CONFIG=development
AWS_DB_HOST=http://localhost:4569
ALGO_URL=http://localhost:8000
AWS_S3_BUCKET=local-bucket
```

However, to be able to run the web-api service, you need the following environment 
variables to be set:
```
CONSUMER_KEY=[.......]
SHARED_SECRET=[.......]
CANVAS_URL=[.......]
CANVAS_KEY=[.......]
```
Where [.......] is the value of the variable.


## Testing Mode
Before running the app in testing mode, make sure that:
1. Algo Service is running
2. Localstack service is running. If you followed the steps on how to run
the algo service, you should have localstack running by default.

The following environment variables is needed:
```
AWS_DB_HOST=http://localhost:4569
```

### Run Tests
Activate the virtual environment and run the command:
```
pytest
```

## Production Mode (In localhost)
To run the service in production mode, several configurations need to take place
in order for the system to start. This will run the web-api service and
will link it to the real AWS S3 and DynamoDB. This is helpful to test how the deployed
system will react. <br>
**Note**: Make sure to run the Algo service in production mode before you proceed.
This is important to have a fully functioning system that is deployed locally but connects
to real AWS services (mainly S3 and DynamoDB). <br>

The following variables are needed to be included in the environment to run
in production mode:
```
FLASK_CONFIG=production
ALGO_URL=http://localhost:8000
CONSUMER_KEY=[.......]
SHARED_SECRET=[.......]
CANVAS_URL=[.......]
CANVAS_KEY=[.......]
AWS_DB_HOST=[......]
AWS_REGION=[......]
S3_BUCKET_NAME=[......]
```
Where [.......] is the value of the variable


# Environment Variables
The following is description of each environment variables needed by all modes,
their intent and how to obtain them:

| Variable | Narrative | Source |
|----------|-----------|--------|
|FLASK_CONFIG|Sets configuration mode to run the server on| Locally: Can be set to 'production', 'test' or 'development'. Defaults to 'development'|
|CONSUMER_KEY|LTI Consumer Key. Used to authenticating the backend with Canvas| Canvas|
|SHARED_SECRET|LTI Shared Secret. Used for authentication with CONSUMER_KEY|Obtained from Canvas|
|CANVAS_URL|Host and port point to target Canvas system|Address can be obtained from deployed Canvas or production one. Please Refer to handover documents on how to setup a canvas server|
|CANVAS_KEY|Canvas Development Key. Gives access to Canvas API|Canvas|
|AWS_DB_HOST|URL to DynamoDB Service|Locally: From localstack service (defaults to http://localhost:4569. AWS Service: Obtained from AWS documentation.|
|AWS_REGIONS|AWS Region for Services provided. For Australia (Sydney data center), it defaults to 'ap-southeast-2'| AWS Documentation|
|ALGO_URL|URL for the Algo URL|Locally: Defaults to http://localhost:8000, for deployment from your system configurations. Refer to Handover document for more details on how setup deployment|
|AWS_S3_BUCKET|S3 Bucket name used to store transferred file|Locally: defaults to 'local-bucket' used with localstack service. AWS: Refer to created bucket name. Please refer to continuous deployment section in handover document|


# Adding new dependencies
Activate virtual environment. Then issue the command
```
pipenv install [library]
```
where [library] is the new library's name
