# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Configuration to start server on different environments
# Configuration depends on path variable 'FLASK_CONFIG'
# Referenced from https://github.com/miguelgrinberg/flasky/blob/master/config.py
import os

from app.models import SubmissionModel


class Config:
    # Configuration for LTI
    CONSUMER_KEY = os.getenv('CONSUMER_KEY')
    SHARED_SECRET = os.getenv("SHARED_SECRET")
    PYLTI_CONFIG = {
        'consumers': {
            CONSUMER_KEY: {
                "secret": SHARED_SECRET
            }
            # Feel free to add more key/secret pairs for other consumers.
        },
        'roles': {
            # Maps values sent in the lti launch value of "roles" to a group
            # Allows you to check LTI.is_role('admin') for your user
            'admin': ['Administrator', 'urn:lti:instrole:ims/lis/Administrator'],
            'student': ['Student', 'urn:lti:instrole:ims/lis/Student']
        }
    }

    # Security Settings
    SECRET_KEY = os.getenv('SECRET_KEY') or os.urandom(24)
    CSRF_ENABLED = True
    SSL_REDIRECT = False

    # Canvas Host and port
    CANVAS_URL = os.getenv('CANVAS_URL')
    CANVAS_KEY = os.getenv('CANVAS_KEY')

    # Algo Microservice
    ALGO_URL = os.getenv('ALGO_URL')

    # AWS information
    S3_BUCKET_NAME = os.getenv('AWS_S3_BUCKET')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    """
    Used for local development
    """
    DEBUG = True

    @staticmethod
    def init_app(app):
        Config.init_app(app)

        # DynamoDB Setting
        SubmissionModel.Meta.host = 'http://localhost:4569'
        if not SubmissionModel.exists():
            SubmissionModel.create_table(wait=True)

        # S3 Setting
        S3_BUCKET_NAME = 'local-bucket'

        # Algo Service
        Config.ALGO_URL = 'http://localhost:8000'


class TestingConfig(Config):
    """
    Used for testing
    """
    TESTING = True

    @staticmethod
    def init_app(app):
        Config.init_app(app)

        # DynamoDB Setting
        SubmissionModel.Meta.table_name = "submission_test"
        SubmissionModel.Meta.host = os.getenv('AWS_DB_HOST') \
                                or "http://dynamodb:8000"

        # S3 Setting
        S3_BUCKET_NAME = 'local-bucket'

        # Algo Service
        Config.ALGO_URL = 'http://localhost:8000'


class ProductionConfig(Config):
    """
    Used for Production Usage (stage and production environment)
    """

    @staticmethod
    def init_app(app):
        Config.init_app(app)

        # running on aws
        AWS_DB_HOST = os.getenv('AWS_DB_HOST')
        AWS_REGION = os.getenv('AWS_REGION')
        SubmissionModel.Meta.host = AWS_DB_HOST
        SubmissionModel.Meta.region = AWS_REGION

        # Print Information
        # TODO Use logging instead
        print("Server Configurations:")
        print(f'Using S3 Bucket: {Config.S3_BUCKET_NAME}')
        print(f'Using DynamoDB with Host: {AWS_DB_HOST}')
        print(f'Using DynamoDB with Region: {AWS_REGION}')
        print(f'Using ALGO Service URL: {Config.ALGO_URL}')
        print(f'Using CANVAS URL: {Config.CANVAS_URL}')


# Configurations passed as a FLASK_CONFIG in system path
config = {
    'development': DevelopmentConfig,
    'test': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}
