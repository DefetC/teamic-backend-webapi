# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Test Models and interaction with database

from app import create_app
from app.models import SubmissionModel


class TestModel:
    # Loads application with test configuration

    @staticmethod
    def sample_submission():
        sub: SubmissionModel = SubmissionModel()
        sub.submission_id = "124"
        sub.student_id = "666666"
        sub.subject_id = "30003"
        sub.workshop_id = "300683"
        sub.assignment_id = "9494"
        sub.comment = "This is a test"
        sub.docs = ["file1.txt", "file2.txt"]
        sub.student_name = "Wenqiang"

        return sub

    @classmethod
    def setup_class(cls):
        # Initialise app with testing settings
        create_app('test')
        # Create fresh table
        if SubmissionModel.exists():
            SubmissionModel.delete_table()
        SubmissionModel.create_table(read_capacity_units=1, write_capacity_units=1,
                                     wait=True)

    @classmethod
    def teardown_class(cls):
        SubmissionModel.delete_table()

    def test_saved(self):
        """
        Test a submission is stored into database
        :return:
        """
        sub = self.sample_submission()
        sub.save()

        # Test submission was persisted
        assert sub.count() == 1

    def test_retrieved(self):
        """
        Test submission can be retrieved
        """
        sub = self.sample_submission()
        result = SubmissionModel.get_submission(assignment_id="9494",
                                                student_id="666666")
        assert result.comment == sub.comment

    def test_retrieved_by_id(self):
        """
        Test submission can be retrieved by id
        """
        sub = self.sample_submission()
        result = SubmissionModel.get_submission_by_id('124')
        assert result.comment == sub.comment

    def test_retrieve_by_subject(self):
        """
        Test submission can be retrieved by subject
        """
        sub = self.sample_submission()
        result = SubmissionModel.get_submissions_by_subject(
            subject_id="30003")
        assert result.next().comment == sub.comment

    def test_retrieve_by_workshop(self):
        """
        Test submission can be retrieved by workshop
        """
        sub = self.sample_submission()
        result = SubmissionModel.get_submissions_by_workshop(
            workshop_id="300683")
        assert result.next().comment == sub.comment
