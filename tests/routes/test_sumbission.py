from datetime import datetime
import requests_mock
import app
from app.models import SubmissionModel
from app.routes.submission import store_submission, rename_filename
import json


def test_receive_submissions(client, mocker):
    """
    Test aim: received submission is stored into db
    and forward to algorithm
    :param client:
    :param mocker:
    :return:
    """
    url = '/submissions'
    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        "submissionId": "1",
        "studentId": "1",
        "studentName": "zhuolun2",
        "assignmentId": "zhaopeng2",
        "subjectId": "1",
        "workshopId": "1",
        "comment": "nothing to comment",
        "files": [
            "known01.txt",
            "known02.txt"
            ],
    }
    mocker.patch('app.routes.submission.forward_to_algo', return_value=True)
    mocker.patch('app.routes.submission.store_submission')
    response = client.post(url, data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    app.routes.submission.store_submission.assert_called_once()


def save_submission():
    sub: SubmissionModel = SubmissionModel()
    sub.submission_id = "smn2"
    sub.student_id = "666666"
    sub.subject_id = "1987"
    sub.workshop_id = "300683"
    sub.assignment_id = "zhaopeng2"
    sub.comment = "This is a test"
    sub.docs = ["file1.txt", "file2.txt"]
    sub.student_name = "Wenqiang"
    sub.save()


def test_query_from_algo(client):
    with requests_mock.mock() as m:
        save_submission()
        algo_data = {
          "data": [
            {
              "assignment_id": "zhaopeng",
              "department_id": "dep1",
              "files": [
                {
                  "filename": "known01.txt",
                  "metrics": [
                    {
                      "metric": 1.0,
                      "metric_history": 0,
                      "metric_name": "cosine"
                    },
                    {
                      "metric": 1.0,
                      "metric_history": 0,
                      "metric_name": "manhattan"
                    }
                  ],
                  "text": "?This chapter gives ..."
                },
                {
                  "filename": "known02.txt",
                  "metrics": [
                    {
                      "metric": 1.0,
                      "metric_history": 0,
                      "metric_name": "cosine"
                    },
                    {
                      "metric": 1.0,
                      "metric_history": 0,
                      "metric_name": "manhattan"
                    }
                  ],
                  "text": "?In this chapter we ..."
                }
              ],
              "student_id": "1",
              "subject_id": "1987",
              "submission_id": "smn2",
              "validated": False,
              "workshop_id": "1"
            }
          ],
          "message": "Retrieved all specified submissions"
        }
        subject_id = '1987'
        m.get('http://127.0.0.1:8000/api/submissions/subject?subject_id='
              + subject_id, text=json.dumps(algo_data))
        url = '/submissions?subjectId=' + subject_id
        response = client.get(url)
        assert response.status_code == 200
        assert algo_data['data'][0]['subject_id'] == subject_id


def test_rename_filename():
    student_id = 'a_student_id'
    assignment_id = 'an_assignment_id'
    prev_filename = 'prev_filename'
    new_filename = rename_filename(student_id, assignment_id, prev_filename)
    names = new_filename.split('-')
    assert names[4] == assignment_id
    assert names[5] == student_id
    assert names[6] == prev_filename
    assert names[3] == datetime.now().strftime("%H:%M:%S")


def test_search_filename(client):
    save_submission()
    student_id = '1'
    assignment_id = 'zhaopeng2'
    url = '/submission/prev?studentId=' + student_id + '&assignmentId=' + assignment_id
    response = client.get(url)
    assert response.status_code == 200
    assert response.json['files'] == ["known01.txt", "known02.txt"]


def test_approve_assignments(client, mocker):
    """
    Test aim: test assignments are approved
    :param client:
    :param mocker:
    :return:
    """
    url = '/submissions/approve'
    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        "submissionId": [
            "1",
            "2"
        ]
    }
    # mocker.patch('app.routes.submission.approve_assignments', return_value=True)
    mocker.patch('requests.put')
    response = client.post(url, data=json.dumps(data), headers=headers)
    print(response)
    assert response.status_code == 200


def test_reject_assignments(client, mocker):
    """
    Test aim: test assignments are approved
    :param client:
    :param mocker:
    :return:
    """
    url = '/submissions/reject'
    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        "submissionId": [
            "1",
            "2"
        ]
    }
    # mocker.patch('app.routes.submission.approve_assignments', return_value=True)
    mocker.patch('requests.put')
    response = client.post(url, data=json.dumps(data), headers=headers)
    print(response)
    assert response.status_code == 200
