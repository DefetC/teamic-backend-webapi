import requests_mock
import json

from app.routes.stats import retrieve_subject_assignment_stats,\
    retrieve_workshop_assignment_stats,\
    retrieve_subject_stats,\
    retrieve_workshop_stats
from config import Config


def retrieve_subject_assignment_stats():
    with requests_mock.mock() as m:
        resp_data = {
            "subjectId": "1987",
            "stats": {
                "highest": 1.0 ,
                "lowest": 1.0,
                "average": 1.0,
                "median": 1.0,
                "totalNum": 1
            }
        }

        subject_id = "1987"
        assignment_id = "zhaopeng"

        headers = {
            'Content-Type': 'application/json'
        }
        json_data = json.dumps(resp_data)
        url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subject_id

        m.get(url, text=json_data, headers=headers)
        assert retrieve_subject_assignment_stats(subject_id, assignment_id)== json_data


def retrieve_workshop_assignment_stats():
    with requests_mock.mock() as m:
        resp_data = {
            "workshopId": "1",
            "stats": {
                "highest": 1.0 ,
                "lowest": 1.0,
                "average": 1.0,
                "median": 1.0,
                "totalNum": 1
            }
        }

        workshop_id = "1"
        assignment_id = "zhaopeng"

        headers = {
            'Content-Type': 'application/json'
        }
        json_data = json.dumps(resp_data)
        url = Config.ALGO_URL + "/api/submissions/workshop?workshop_id=" + workshop_id

        m.get(url, text=json_data, headers=headers)
        assert retrieve_workshop_assignment_stats(workshop_id, assignment_id)== json_data


def retrieve_subject_stats():
    with requests_mock.mock() as m:
        resp_data = {
            "subjectId": "1987",
            "stats": {
                "highest": 1.0 ,
                "lowest": 1.0,
                "average": 1.0,
                "median": 1.0,
                "totalNum": 1
            }
        }

        subject_id = "1987"

        headers = {
            'Content-Type': 'application/json'
        }
        json_data = json.dumps(resp_data)
        url = Config.ALGO_URL + "/api/submissions/subject?subject_id=" + subject_id

        m.get(url, text=json_data, headers=headers)
        assert retrieve_workshop_assignment_stats(subject_id)== json_data

def retrieve_workshop_stats():
    with requests_mock.mock() as m:
        resp_data = {
            "workshopId": "1",
            "stats": {
                "highest": 1.0 ,
                "lowest": 1.0,
                "average": 1.0,
                "median": 1.0,
                "totalNum": 1
            }
        }

        workshop_id = "1"

        headers = {
            'Content-Type': 'application/json'
        }
        json_data = json.dumps(resp_data)
        url = Config.ALGO_URL + "/api/submissions/workshop?workshop_id=" + workshop_id

        m.get(url, text=json_data, headers=headers)
        assert retrieve_workshop_assignment_stats(workshop_id)== json_data