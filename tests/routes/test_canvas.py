import requests_mock
import app
import json

from app.routes.canvas import look_up_workshop_id, look_up_type
from config import Config


def test_look_up_workshop_id():
    with requests_mock.mock() as m:
        resp_data = [{
            "id": 1,
            "user_id": 1,
            "course_id": 1,
            "type": "TeacherEnrollment",
            "course_section_id": 1,
            "total_activity_time": 876,
        }]
        headers = {
            'Content-Type': 'application/json',
            'Authorization':
                'Bearer ' + Config.CANVAS_KEY
        }
        json_data = json.dumps(resp_data)
        subject_id = '1'
        user_id = '1'
        url = 'http://165.22.220.225:3000/api/v1/courses/' + subject_id \
              + '/enrollments?user_id=' + user_id
        m.get(url, text=json_data, headers=headers)
        assert look_up_workshop_id(subject_id, user_id) == resp_data[0]['course_section_id']


def test_look_up_enrollment_type(mocker):
    with requests_mock.mock() as m:
        resp_data = [{
            "id": 1,
            "user_id": 1,
            "course_id": 1,
            "type": "TeacherEnrollment",
            "course_section_id": 1,
            "total_activity_time": 876,
        }]
        headers = {
            'Content-Type': 'application/json',
            'Authorization':
                'Bearer ' + Config.CANVAS_KEY
        }
        json_data = json.dumps(resp_data)
        subject_id = '1'
        user_id = '1'
        url = 'http://165.22.220.225:3000/api/v1/courses/' + subject_id \
              + '/enrollments?user_id=' + user_id
        m.get(url, text=json_data, headers=headers)
        assert look_up_type(subject_id, user_id) == resp_data[0]['type']
