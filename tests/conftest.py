# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# The following section is used to load python path to pytest
# This helps to run all tests using the 'pytest' command.
# If this section is not added, you can test by using:
# 'python -m pytest'
import sys
from os.path import dirname as d
from os.path import abspath, join

from app.routes import bp

root_dir = d(d(abspath(__file__)))
sys.path.append(root_dir)

# Configures the application to be used by different test units
import pytest
from app import create_app, config


@pytest.fixture
def client():
    app = create_app('test')
    app.register_blueprint(bp, url_prefix='/')
    for rule in app.url_map.iter_rules():
        print(rule)
    return app.test_client()
